package tests.unit;

import org.junit.Test;

import domain.character.CharacterEntity;

import static abstraction.Constants.IMAGE_CHAR1;
import static abstraction.Constants.IMAGE_CHAR2;
import static abstraction.Constants.IMAGE_CHAR_JUMP;
import static org.junit.Assert.assertEquals;

public class CharacterTest {

    @Test
    public void checkCharacterFirstImage() {
        CharacterEntity character = new CharacterEntity();

        assertEquals(IMAGE_CHAR1, character.getCharacterImage());
    }

    @Test
    public void checkCharacterSecondImage() {
        CharacterEntity character = new CharacterEntity();

        character.move();

        assertEquals(IMAGE_CHAR2, character.getCharacterImage());
    }

    @Test
    public void checkCharacterImageOnJump() {
        CharacterEntity character = new CharacterEntity();

        character.move();
        character.startJump();

        assertEquals(IMAGE_CHAR_JUMP, character.getCharacterImage());
    }

    @Test
    public void checkCharacterImageAfterJump() {
        CharacterEntity character = new CharacterEntity();

        character.move();
        character.startJump();
        character.fallIsPressed();
        character.move();
        character.move();

        assertEquals(IMAGE_CHAR1, character.getCharacterImage());
    }

    @Test
    public void checkCharacterHeightOnBegin() {
        CharacterEntity character = new CharacterEntity();
        assertEquals(0, character.getHeight());
    }

    @Test
    public void checkCharacterHeightAfterStartJump() {
        CharacterEntity character = new CharacterEntity();

        character.startJump();

        assertEquals(20, character.getHeight());
    }

    @Test
    public void checkCharacterHeightAfterReachHighestPosition() {
        CharacterEntity character = new CharacterEntity();

        character.startJump();
        for(int i = 0; i < 17; i++) {
            character.move();
        }
        assertEquals(-20, character.getHeight());
    }

    @Test
    public void checkCharacterHeightAfterFallIsPressed() {
        CharacterEntity character = new CharacterEntity();

        character.startJump();
        for(int i = 0; i < 17; i++) {
            character.move();
        }
        character.fallIsPressed();
        character.move();

        assertEquals(-40, character.getHeight());
    }

    @Test
    public void checkCharacterHeightAfterJump() {
        CharacterEntity character = new CharacterEntity();

        character.startJump();
        for(int i = 0; i < 17; i++) {
            character.move();
        }
        character.fallIsPressed();
        for(int i = 0; i < 17; i++) {
            character.move();
        }

        assertEquals(0, character.getHeight());
    }
}
