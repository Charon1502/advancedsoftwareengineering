package tests.unit;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import domain.barrier.BarrierAggregate;
import domain.character.CharacterEntity;
import domain.barrier.EnemyEntity;
import application.services.GameEndingConditionService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameEndingConditionTest {

    private CharacterEntity characterEntity;
    private List<BarrierAggregate> barrierAggregate = new ArrayList<>();
    private GameEndingConditionService gameEndingConditionService;

    @Test
    public void checkGameEndingConditionOnBegin() {

        characterEntity = new CharacterEntity();
        barrierAggregate.add(new EnemyEntity());
        gameEndingConditionService = new GameEndingConditionService();

        assertFalse(gameEndingConditionService.isGameOver(characterEntity, barrierAggregate));
    }

    @Test
    public void checkGameEndingConditionOnEnd() {

        characterEntity = new CharacterEntity();
        barrierAggregate.add(new EnemyEntity());
        gameEndingConditionService = new GameEndingConditionService();

        for (int i = 110; i > 24; i--) {
            barrierAggregate.get(0).move();
        }

        assertTrue(gameEndingConditionService.isGameOver(characterEntity, barrierAggregate));
    }
}
