package tests.unit;

import org.junit.Test;

import domain.barrier.BarrierAggregate;
import domain.barrier.EnemyEntity;
import domain.barrier.BarrierDetailValueObject;
import domain.barrier.ValidMovementSpeed;

import static abstraction.Constants.IMAGE_ENEMY1;
import static abstraction.Constants.IMAGE_ENEMY2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BarrierAggregateTest {

    @Test
    public void checkBarrierAggregateFirstImageForEnemy() {
        BarrierAggregate barrierAggregate = new EnemyEntity();

        barrierAggregate.move();

        assertEquals(IMAGE_ENEMY1, barrierAggregate.getActiveImage());
    }

    @Test
    public void checkBarrierAggregateSecondImageForEnemy() {
        BarrierAggregate barrierAggregate = new EnemyEntity();

        barrierAggregate.move();
        barrierAggregate.move();

        assertEquals(IMAGE_ENEMY2, barrierAggregate.getActiveImage());
    }

    @Test
    public void checkBarrierAggregateMovementLogicForEnemyOnBegin() {
        BarrierAggregate barrierAggregate = new EnemyEntity();

        assertEquals(110, barrierAggregate.getIteratorsUntilRemove());
    }

    @Test
    public void checkBarrierAggregateMovementLogicForEnemyOnEnd() {
        BarrierAggregate barrierAggregate = new EnemyEntity();

        for(int i = 0; i < 20; i++) {
            barrierAggregate.move();
        }

        assertEquals(90, barrierAggregate.getIteratorsUntilRemove());
    }

    @Test
    public void checkBarrierDetailValueObjectFilledProperlyForEnemy() {
        BarrierAggregate barrierAggregate = new EnemyEntity();

        assertTrue(barrierAggregate.getBarrierDetailValueObject().equals(new BarrierDetailValueObject(ValidMovementSpeed.MOVEMENT_SPEED_ENEMY, IMAGE_ENEMY1, IMAGE_ENEMY2, 25, 15)));

    }

}
