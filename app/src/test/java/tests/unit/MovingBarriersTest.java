package tests.unit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import domain.barrier.BarrierAggregate;
import application.services.MovingInSingleGameLoopService;
import domain.character.CharacterEntity;
import domain.barrier.EnemyEntity;

import static abstraction.Constants.IMAGE_ENEMY1;
import static abstraction.Constants.IMAGE_ENEMY2;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class MovingBarriersTest {

    @Mock
    private CharacterEntity characterEntity;

    private List<BarrierAggregate> barrierAggregate = new ArrayList<>();

    @InjectMocks
    private MovingInSingleGameLoopService movingInSingleGameLoopService;

    @Before
    public void setUp() {
        characterEntity = mock(CharacterEntity.class);
    }

    @Test
    public void checkMovingBarriersInitial() {

        barrierAggregate.add(new EnemyEntity());
        movingInSingleGameLoopService = new MovingInSingleGameLoopService();

        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterEntity, barrierAggregate);

        assertEquals(IMAGE_ENEMY1, barrierAggregate.get(0).getActiveImage());
    }

    @Test
    public void checkMovingBarriersSwitchedImage() {

        barrierAggregate.add(new EnemyEntity());
        movingInSingleGameLoopService = new MovingInSingleGameLoopService();

        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterEntity, barrierAggregate);
        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterEntity, barrierAggregate);

        assertEquals(IMAGE_ENEMY2, barrierAggregate.get(0).getActiveImage());
    }

    @Test
    public void checkMovingBarriersDelete() {

        barrierAggregate.add(new EnemyEntity());
        movingInSingleGameLoopService = new MovingInSingleGameLoopService();

        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterEntity, barrierAggregate);
        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterEntity, barrierAggregate);
        for(int i = barrierAggregate.get(0).getIteratorsUntilRemove(); i > 1; i--) {
            barrierAggregate.get(0).move();
        }
        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterEntity, barrierAggregate);

        assertEquals(0, barrierAggregate.size());
    }
}
