package abstraction;

import java.util.concurrent.atomic.AtomicInteger;

public final class IdGenerator {

    protected static final AtomicInteger barrierId = new AtomicInteger();

    public static int generateNextId() {
        return barrierId.getAndIncrement();
    }
}
