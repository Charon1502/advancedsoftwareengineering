package abstraction;

import adapters.R;

public class Constants {

    // Konstanten für den Zugriff auf Bilder
    public static final int IMAGE_CHAR1 = R.drawable.worm_v1;
    public static final int IMAGE_CHAR2 = R.drawable.worm_v2;
    public static final int IMAGE_CHAR_JUMP = R.drawable.worm_jump;
    public static final int IMAGE_ENEMY1 = R.drawable.enemy_v1;
    public static final int IMAGE_ENEMY2 = R.drawable.enemy_v2;
    public static final int IMAGE_KAMIKAZE = R.drawable.kamikaze;
}
