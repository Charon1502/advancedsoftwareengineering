package plugins;

import android.view.View;
import android.widget.TextView;

import adapters.R;

public class SetGameEndView {

    public SetGameEndView(MainActivity mainActivity, int score) {
        mainActivity.runOnUiThread(() -> {
            TextView textView = mainActivity.findViewById(R.id.endGameTextView);
            textView.append(" " + score);
            textView.setVisibility(View.VISIBLE);
        });
    }
}
