package plugins;

import android.view.View;

import domain.character.CharacterEntity;

public class ActualizeCharacterView {

    private final MainActivity mainActivity;

    public ActualizeCharacterView(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void actualizeCharacterView(CharacterEntity character) {
        View characterView = mainActivity.findViewById(character.getCharacterId());
        characterView.setY(characterView.getY() - character.getHeight());
        characterView.setBackgroundResource(character.getCharacterImage());
    }
}
