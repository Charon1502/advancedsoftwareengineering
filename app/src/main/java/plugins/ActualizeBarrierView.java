package plugins;

import android.view.View;

import java.util.List;

import domain.barrier.BarrierAggregate;

public class ActualizeBarrierView {

    private final MainActivity mainActivity;

    public ActualizeBarrierView(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void actualizeBarrierView(List<BarrierAggregate> barriers) {
        barriers.forEach(barrier -> {
            View view = mainActivity.findViewById(barrier.getBarrierId());
            if (view != null) {
                view.setX(view.getX() - barrier.getBarrierDetailValueObject().getValidMovementSpeed().getMovementSpeed());
            } else {
                new CreateNewImageViewForEnemy().createNewImageViewForEnemy(mainActivity, barrier.getActiveImage(), barrier.getBarrierId());
            }
        });
    }
}
