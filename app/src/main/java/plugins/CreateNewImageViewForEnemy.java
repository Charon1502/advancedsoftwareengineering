package plugins;

import android.widget.ImageView;
import android.widget.LinearLayout;

public class CreateNewImageViewForEnemy {

    public CreateNewImageViewForEnemy(){}

    public void createNewImageViewForEnemy(MainActivity mainActivity, int imageRessource, int id) {
        ImageView barrierView = new ImageView(mainActivity.getApplicationContext());
        barrierView.setX(1920);
        barrierView.setY(620);
        barrierView.setLayoutParams(new LinearLayout.LayoutParams(150, 150));
        barrierView.setImageResource(imageRessource);
        barrierView.setId(id);
        mainActivity.addContentView(barrierView, barrierView.getLayoutParams());
    }
}
