package plugins;

import java.util.List;

import adapters.ConcreteObservableForGameController;
import adapters.GameControllerObserver;
import application.GameLoop;
import domain.barrier.BarrierAggregate;
import domain.character.CharacterEntity;

public class ObserverForGameController implements GameControllerObserver<ConcreteObservableForGameController> {

    private final MainActivity mainActivity;
    private final ActualizeCharacterView actualizeCharacterView;
    private final ActualizeBarrierView actualizeBarrierView;
    private final ActualizeScoreView actualizeScoreView;

    public ObserverForGameController(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.actualizeCharacterView = new ActualizeCharacterView(mainActivity);
        this.actualizeBarrierView = new ActualizeBarrierView(mainActivity);
        this.actualizeScoreView = new ActualizeScoreView(mainActivity);
    }

    @Override
    public void actualizeAllChangedGameViews(ConcreteObservableForGameController observable) {
        GameLoop gameLoop = observable.getGameLoop();
        if(!observable.isCharacterIdSet()) {
            observable.setCharacterIdSet(true);
            new SetCharacterViewIdOnGameStart(mainActivity, gameLoop.getCharacterRepo().getCharacter().getCharacterId());
        }
        mainActivity.runOnUiThread(() -> actualizeAllEveryLoopChangedViews(gameLoop.getCharacterRepo().getCharacter(),gameLoop.getBarrierRepo().getBarrierAggregateList(), gameLoop.getScore()));

        if (observable.isGameOver()) {
            new SetGameEndView(mainActivity, gameLoop.getScore());
            observable.removeObserver(this);
        }
    }

    public void actualizeAllEveryLoopChangedViews(CharacterEntity character, List<BarrierAggregate> barriers, int score) {
        actualizeCharacterView.actualizeCharacterView(character);
        actualizeBarrierView.actualizeBarrierView(barriers);
        actualizeScoreView.actualizeScoreView(score);
    }
}
