package plugins;

import adapters.R;

public class SetCharacterViewIdOnGameStart {

    public SetCharacterViewIdOnGameStart(MainActivity mainActivity, int characterId) {
        mainActivity.runOnUiThread(() -> mainActivity.findViewById(R.id.character).setId(characterId));
    }
}
