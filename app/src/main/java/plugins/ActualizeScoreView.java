package plugins;

import android.widget.TextView;

import adapters.R;

public class ActualizeScoreView {

    private final MainActivity mainActivity;

    public ActualizeScoreView(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void actualizeScoreView(int score) {
        TextView view = mainActivity.findViewById(R.id.score);
        view.setText(R.string.score);
        view.append(" " + score);
    }
}
