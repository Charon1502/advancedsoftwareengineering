package plugins;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import adapters.ConcreteObservableForGameController;
import adapters.GameController;
import adapters.R;

public final class MainActivity extends AppCompatActivity {

    private GameController gameController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ConcreteObservableForGameController observableForGameController = new ConcreteObservableForGameController();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        observableForGameController.addObserver(new ObserverForGameController(this));
        gameController = new GameController(observableForGameController);
    }

    public void resolveJumpUp(View buttonView) {
        gameController.jumpButtonPressed(false);
    }

    public void resolveJumpDown(View buttonView) {
        gameController.jumpButtonPressed(true);
    }
}