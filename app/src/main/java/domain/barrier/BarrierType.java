package domain.barrier;

public enum BarrierType {
    ENEMY("enemy"), KAMIKAZE("kamikaze");

    private final String barrierType;

    BarrierType(String barrierType) {
        this.barrierType = barrierType;
    }

    @Override
    public String toString() {
        return barrierType;
    }
}
