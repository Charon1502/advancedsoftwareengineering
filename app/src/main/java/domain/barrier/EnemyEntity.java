package domain.barrier;

import static abstraction.Constants.IMAGE_ENEMY1;
import static abstraction.Constants.IMAGE_ENEMY2;

public final class EnemyEntity extends BarrierAggregate {

    /**
     * Konstruktor
     */
    public EnemyEntity() {
        super(BarrierType.ENEMY, 110, ValidMovementSpeed.MOVEMENT_SPEED_ENEMY, IMAGE_ENEMY1, IMAGE_ENEMY2, 25, 15);
    }
}
