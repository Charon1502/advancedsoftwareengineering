package domain.barrier;

import java.util.List;

public interface IBarrierRepo {
    List<BarrierAggregate> getBarrierAggregateList();
    void addBarrierAggregateToList(BarrierAggregate barrierAggregate);
    void removeBarrierAggregateFromList(BarrierAggregate barrierAggregate);
}
