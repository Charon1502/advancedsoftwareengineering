package domain.barrier;

public enum ValidMovementSpeed {
    MOVEMENT_SPEED_ENEMY(20), MOVEMENT_SPEED_KAMIKAZE(40);

    private final int movementSpeed;

    ValidMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public int getMovementSpeed() {
        return movementSpeed;
    }
}
