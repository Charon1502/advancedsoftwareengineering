package domain.barrier;

import java.util.Objects;

import abstraction.IdGenerator;

public abstract class BarrierAggregate {

    private final int barrierId;
    private final BarrierType barrierType;
    protected BarrierDetailValueObject barrierDetailValueObject;
    protected int iteratorsUntilRemove;
    protected boolean changeImage;
    private int activeImage;

    public BarrierAggregate(BarrierType barrierType, int iteratorsUntilRemove, ValidMovementSpeed movementSpeed, int firstBarrierImage, int secondBarrierImage, int iteratorsUntilRemoveHitBoxBegin, int iteratorsUntilRemoveHitBoxEnd) {
        this.barrierId = IdGenerator.generateNextId();
        this.barrierType = barrierType;
        this.iteratorsUntilRemove = iteratorsUntilRemove;
        this.changeImage = true;
        this.barrierDetailValueObject = new BarrierDetailValueObject(movementSpeed, firstBarrierImage, secondBarrierImage, iteratorsUntilRemoveHitBoxBegin, iteratorsUntilRemoveHitBoxEnd);
    }

    public void move() {
        if (changeImage) {
            activeImage = barrierDetailValueObject.getFirstBarrierImage();
        } else {
            activeImage = barrierDetailValueObject.getSecondBarrierImage();
        }
        changeImage = !changeImage;
        iteratorsUntilRemove--;
    }

    public static BarrierAggregate generateBarrierOfRandomType() {
        int randomNumber = (int) (Math.random() * 2);
        switch (randomNumber) {
            case 0:
                return new KamikazeEntity();
            case 1:
            default:
                return new EnemyEntity();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BarrierAggregate that = (BarrierAggregate) o;
        return barrierId == that.barrierId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(barrierId);
    }

    public int getBarrierId() {
        return barrierId;
    }

    public int getActiveImage() {
        return activeImage;
    }

    public int getIteratorsUntilRemove() {
        return iteratorsUntilRemove;
    }

    public BarrierDetailValueObject getBarrierDetailValueObject() {
        return barrierDetailValueObject;
    }
}
