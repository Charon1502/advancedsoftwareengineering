package domain.barrier;

import java.util.Objects;

public final class BarrierDetailValueObject {
    private final ValidMovementSpeed movementSpeed;
    private final int firstBarrierImage;
    private final int secondBarrierImage;
    private final int iteratorsUntilRemoveHitBoxBegin;
    private final int iteratorsUntilRemoveHitBoxEnd;

    public BarrierDetailValueObject(ValidMovementSpeed movementSpeed, int firstBarrierImage, int secondBarrierImage, int iteratorsUntilRemoveHitBoxBegin, int iteratorsUntilRemoveHitBoxEnd) {
        this.movementSpeed = movementSpeed;
        this.firstBarrierImage = firstBarrierImage;
        this.secondBarrierImage = secondBarrierImage;
        this.iteratorsUntilRemoveHitBoxBegin = iteratorsUntilRemoveHitBoxBegin;
        this.iteratorsUntilRemoveHitBoxEnd = iteratorsUntilRemoveHitBoxEnd;


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BarrierDetailValueObject that = (BarrierDetailValueObject) o;
        return firstBarrierImage == that.firstBarrierImage &&
                secondBarrierImage == that.secondBarrierImage &&
                iteratorsUntilRemoveHitBoxBegin == that.iteratorsUntilRemoveHitBoxBegin &&
                iteratorsUntilRemoveHitBoxEnd == that.iteratorsUntilRemoveHitBoxEnd &&
                movementSpeed == that.movementSpeed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(movementSpeed, firstBarrierImage, secondBarrierImage, iteratorsUntilRemoveHitBoxBegin, iteratorsUntilRemoveHitBoxEnd);
    }

    public ValidMovementSpeed getValidMovementSpeed() {
        return movementSpeed;
    }

    public int getFirstBarrierImage() {
        return firstBarrierImage;
    }

    public int getSecondBarrierImage() {
        return secondBarrierImage;
    }

    public int getIteratorsUntilRemoveHitBoxBegin() {
        return iteratorsUntilRemoveHitBoxBegin;
    }

    public int getIteratorsUntilRemoveHitBoxEnd() {
        return iteratorsUntilRemoveHitBoxEnd;
    }
}

