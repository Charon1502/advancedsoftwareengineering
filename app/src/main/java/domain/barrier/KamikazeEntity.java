package domain.barrier;

import static abstraction.Constants.IMAGE_KAMIKAZE;

public class KamikazeEntity extends BarrierAggregate {

    public KamikazeEntity() {
        super(BarrierType.KAMIKAZE, 55, ValidMovementSpeed.MOVEMENT_SPEED_KAMIKAZE, IMAGE_KAMIKAZE, IMAGE_KAMIKAZE, 13, 5);
    }
}
