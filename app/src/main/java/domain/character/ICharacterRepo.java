package domain.character;

public interface ICharacterRepo {
    CharacterEntity getCharacter();
}
