package domain.character;

import java.util.Objects;

import abstraction.IdGenerator;

import static abstraction.Constants.IMAGE_CHAR1;
import static abstraction.Constants.IMAGE_CHAR2;
import static abstraction.Constants.IMAGE_CHAR_JUMP;

public final class CharacterEntity {

    private final int characterId;
    private final CharacterDetailValueObject characterDetailValueObject;
    private int height;
    private int heightUseCounter;
    private int characterImage;
    private boolean isFallPressed;
    private boolean isFalling;

    /**
     * Konstruktor
     */
    public CharacterEntity() {
        this.characterId = IdGenerator.generateNextId();
        this.characterDetailValueObject = new CharacterDetailValueObject();
        this.height = 0;
        this.heightUseCounter = 0;
        this.characterImage = IMAGE_CHAR1;
        this.isFallPressed = false;

    }

    /**
     * Führt die Bewegung des Characters aus & ändert das angezeigte Bild
     */
    public void move() {
        if (height != 0) {
            if (isFallPressed) {
                resolveFasterFall();
            } else {
                resolveJump();
            }
            return;
        }
        if (this.characterImage == IMAGE_CHAR1) {
            this.characterImage = IMAGE_CHAR2;
            return;
        }
        this.characterImage = IMAGE_CHAR1;
    }

    /**
     * Beginnt die Sprunganimation
     */
    public void startJump() {
        if (0 == height) {
            isFalling = false;
            height = characterDetailValueObject.getHeightParameter();
            this.characterImage = IMAGE_CHAR_JUMP;
        }
    }

    /**
     * Ermöglicht schnelleres Landen bei einem Sprung
     */
    public void fallIsPressed() {
        if (!isFallPressed) {
            heightUseCounter += 1;
        }
        isFallPressed = true;

    }

    /**
     * Ermittelt die Funktion für einen "normalen" Sprung
     */
    public void resolveJump() {
        if (heightUseCounter >= 16)
            isFalling = true;
        if (!isFalling) {
            height = characterDetailValueObject.getHeightParameter();
            heightUseCounter++;
            return;
        }
        height = -characterDetailValueObject.getHeightParameter();
        heightUseCounter--;
        if (heightUseCounter < 0) {
            height = 0;
            heightUseCounter = 0;
        }
    }

    /**
     * Wenn während dem Springen die "Fall" Taste betätigt wird, wird der Sprung verkürzt
     */
    private void resolveFasterFall() {
        height = -(characterDetailValueObject.getHeightParameter() * 2);
        heightUseCounter -= 2;
        if (heightUseCounter == 0) {
            height = -characterDetailValueObject.getHeightParameter();
        }
        if (heightUseCounter < 0) {
            heightUseCounter = 0;
            height = 0;
            isFallPressed = false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CharacterEntity that = (CharacterEntity) o;
        return characterId == that.characterId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(characterId);
    }

    public int getCharacterId() {
        return characterId;
    }

    public int getCharacterImage() { return characterImage;}

    public int getHeight() {
        return height;
    }

    public boolean isAboveNormalEnemies() {
        return heightUseCounter >= 4;
    }
}
