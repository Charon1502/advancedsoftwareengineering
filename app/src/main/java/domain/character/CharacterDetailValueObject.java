package domain.character;

import java.util.Objects;

public final class CharacterDetailValueObject {

    private final int heightParameter;

    public CharacterDetailValueObject() {
        heightParameter = 20;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CharacterDetailValueObject that = (CharacterDetailValueObject) o;
        return heightParameter == that.heightParameter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(heightParameter);
    }

    public int getHeightParameter() {
        return heightParameter;
    }
}
