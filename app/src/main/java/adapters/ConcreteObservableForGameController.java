package adapters;

import java.util.HashSet;
import java.util.Set;

import application.GameLoop;

public class ConcreteObservableForGameController implements GameControllerObservable {

    private GameLoop gameLoop;
    private boolean isGameOver = false;
    private boolean characterIdSet = false;
    private final Set<GameControllerObserver> observers = new HashSet<>();

    @Override
    public void addObserver(GameControllerObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(GameControllerObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for(GameControllerObserver observer : observers) {
            observer.actualizeAllChangedGameViews(this);
        }
    }

    public void setGameLoop(GameLoop gameLoop) {
        this.gameLoop = gameLoop;
        this.notifyObserver();
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
        this.notifyObserver();
    }

    public void setCharacterIdSet(boolean characterIdSet) {
        this.characterIdSet = characterIdSet;
    }

    public GameLoop getGameLoop() {
        return gameLoop;
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public boolean isCharacterIdSet() {
        return characterIdSet;
    }
}
