package adapters;

public interface GameControllerObserver<T extends GameControllerObservable>{
    void actualizeAllChangedGameViews(T observable);
}
