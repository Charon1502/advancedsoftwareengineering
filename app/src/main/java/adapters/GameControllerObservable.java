package adapters;

public interface GameControllerObservable {
    void addObserver(GameControllerObserver observer);
    void removeObserver(GameControllerObserver observer);
    void notifyObserver();

}
