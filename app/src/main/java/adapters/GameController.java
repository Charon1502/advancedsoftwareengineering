package adapters;

import java.util.Timer;
import java.util.TimerTask;

import application.GameLoop;

public final class GameController{

    private final GameLoop gameLoop;

    public GameController(ConcreteObservableForGameController observableForGameController) {
        gameLoop = new GameLoop();

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!observableForGameController.isGameOver()) {
                    gameLoop.resolveSingleGameLoop();
                    observableForGameController.setGameLoop(gameLoop);
                    if (gameLoop.isGameIsOver()) {
                        observableForGameController.setGameOver(true);
                    }
                } else
                    this.cancel();
            }
        }, 0, 100);
    }

    public void jumpButtonPressed(boolean isFallPressed) {
        gameLoop.getJumpButtonPressed().jumpButtonPressed(gameLoop.getCharacterRepo().getCharacter(), isFallPressed);
    }
}
