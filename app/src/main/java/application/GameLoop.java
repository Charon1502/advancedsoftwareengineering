package application;

import domain.barrier.BarrierAggregate;
import application.services.GameEndingConditionService;
import application.services.GenerateBarrierService;
import application.services.MovingInSingleGameLoopService;
import application.services.ResolvePressedJumpButtonService;

public class GameLoop {

    private final GenerateBarrierService generateBarrierService;
    private final ResolvePressedJumpButtonService jumpButtonPressed;
    private final GameEndingConditionService isGameOver;
    private final MovingInSingleGameLoopService movingInSingleGameLoopService;

    private final CharacterRepo characterRepo;
    private final BarrierRepo barrierRepo;
    private int score;
    private boolean gameIsOver;

    public GameLoop() {
        score = 0;
        gameIsOver = false;
        characterRepo = new CharacterRepo();
        barrierRepo = new BarrierRepo();
        generateBarrierService = new GenerateBarrierService();
        jumpButtonPressed = new ResolvePressedJumpButtonService();
        isGameOver = new GameEndingConditionService();
        movingInSingleGameLoopService = new MovingInSingleGameLoopService();
    }

    public void resolveSingleGameLoop() {
        score++;
        BarrierAggregate barrier = generateBarrierService.calculatePermissionToSpawnBarrier();
        if (barrier != null) {
            barrierRepo.addBarrierAggregateToList(barrier);
        }
        movingInSingleGameLoopService.resolveSingleGameLoopMovement(characterRepo.getCharacter(), barrierRepo.getBarrierAggregateList());
        if (isGameOver.isGameOver(characterRepo.getCharacter(), barrierRepo.getBarrierAggregateList()))
            gameIsOver = true;
    }

    public CharacterRepo getCharacterRepo() {
        return characterRepo;
    }

    public BarrierRepo getBarrierRepo() {
        return barrierRepo;
    }

    public ResolvePressedJumpButtonService getJumpButtonPressed() {
        return jumpButtonPressed;
    }

    public int getScore() {
        return this.score;
    }

    public boolean isGameIsOver() {
        return gameIsOver;
    }
}
