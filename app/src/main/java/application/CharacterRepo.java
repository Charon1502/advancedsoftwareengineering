package application;

import domain.character.CharacterEntity;
import domain.character.ICharacterRepo;

public class CharacterRepo implements ICharacterRepo {

    private CharacterEntity character;

    public CharacterRepo() {
        this.character = new CharacterEntity();
    }

    @Override
    public CharacterEntity getCharacter() {
        return character;
    }
}
