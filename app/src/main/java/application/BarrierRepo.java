package application;

import java.util.ArrayList;
import java.util.List;

import domain.barrier.BarrierAggregate;
import domain.barrier.IBarrierRepo;

public class BarrierRepo implements IBarrierRepo {

    private List<BarrierAggregate> barrierAggregateList = new ArrayList<>();

    @Override
    public List<BarrierAggregate> getBarrierAggregateList() {
        return barrierAggregateList;
    }

    @Override
    public void addBarrierAggregateToList(BarrierAggregate barrierAggregate) {
        barrierAggregateList.add(barrierAggregate);
    }

    @Override
    public void removeBarrierAggregateFromList(BarrierAggregate barrierAggregate) {
        barrierAggregateList.remove(barrierAggregate);
    }
}
