package application.services;

import java.util.ConcurrentModificationException;
import java.util.List;

import domain.barrier.BarrierAggregate;
import domain.character.CharacterEntity;

public class MovingInSingleGameLoopService {

    public void resolveSingleGameLoopMovement(CharacterEntity characterEntity, List<BarrierAggregate> barrierAggregates) {
        characterEntity.move();
        try {
            barrierAggregates.forEach(barrierAggregate -> {
                barrierAggregate.move();
                if (barrierAggregate.getIteratorsUntilRemove() == 0) {
                    barrierAggregates.remove(barrierAggregate);
                }
            });
        } catch (ConcurrentModificationException e) {

        }
    }
}
