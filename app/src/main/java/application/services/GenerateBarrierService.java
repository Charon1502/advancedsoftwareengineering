package application.services;

import domain.barrier.BarrierAggregate;

public final class GenerateBarrierService {

    private int counter;
    private boolean isSpawnPermitted;

    public GenerateBarrierService() {
        counter = 0;
        isSpawnPermitted = true;
    }

    /**
     * Ermittelt, ob ein Hindernis generiert werden soll
     */
    public BarrierAggregate calculatePermissionToSpawnBarrier() {
        if (++counter == 25) {
            counter = 0;
            if (isSpawnPermitted && Math.random() < 0.75) {
                isSpawnPermitted = false;
                return BarrierAggregate.generateBarrierOfRandomType();
            }
            isSpawnPermitted = true;
        }
        return null;
    }
}
