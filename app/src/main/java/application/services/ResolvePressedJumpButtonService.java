package application.services;

import domain.character.CharacterEntity;

public class ResolvePressedJumpButtonService {

    public void jumpButtonPressed(CharacterEntity characterEntity, boolean isFallPressed) {
        if (characterEntity.getHeight() == 0 && !isFallPressed) {
            characterEntity.startJump();
        }
        if (characterEntity.getHeight() != 0 && isFallPressed) {
            characterEntity.fallIsPressed();
        }
    }
}
