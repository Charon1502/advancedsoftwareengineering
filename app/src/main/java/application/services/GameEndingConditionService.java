package application.services;

import java.util.List;

import domain.barrier.BarrierAggregate;
import domain.character.CharacterEntity;

public class GameEndingConditionService {

    private boolean isEnemyHit;


    public boolean isGameOver(CharacterEntity characterEntity, List<BarrierAggregate> barrierAggregates) {
        return resolveIfEnemyIsHitted(characterEntity, barrierAggregates);
    }

    private boolean resolveIfEnemyIsHitted(CharacterEntity characterEntity, List<BarrierAggregate> barrierAggregates) {
        isEnemyHit = false;
        barrierAggregates.forEach(barrier -> {
            if (barrier.getIteratorsUntilRemove() < barrier.getBarrierDetailValueObject().getIteratorsUntilRemoveHitBoxBegin()
                    && barrier.getIteratorsUntilRemove() > barrier.getBarrierDetailValueObject().getIteratorsUntilRemoveHitBoxEnd()
                    && !characterEntity.isAboveNormalEnemies()) {
                isEnemyHit = true;
            }
        });
        return isEnemyHit;
    }
}
